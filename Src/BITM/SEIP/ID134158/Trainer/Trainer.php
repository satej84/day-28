<?php

namespace App\BITM\SEIP\ID134158\Trainer;

use PDO;

use App\BITM\SEIP\ID134158\Utility\Utility;
use App\BITM\SEIP\ID134158\Massage\Massage;

class Trainer
{
    public $id = '';
    public $name = '';
    public $email = '';
    public $nickName = '';
    public $serverName = 'localhost';
    public $dbname = 'labexam8';
    public $user = 'root';
    public $pass = '';
    public $conn;

    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->dbname", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = '')
    {
        if (array_key_exists('tname', $data) && !empty($data)) {
            $this->name = $data['tname'];
        }
        if (array_key_exists('temail', $data) && !empty($data)) {
            $this->email = $data['temail'];
        }
        if (array_key_exists('tnick', $data) && !empty($data)) {
            $this->nickName = $data['tnick'];
        }
        if (array_key_exists('id', $data) && !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO `trainer` (`trainer_name`, `trainer_email`, `trainer_nick_name`) VALUES (:tName, :tEmail, :tNickName)";
        $stmt = $this->conn->prepare($query);
        $response=$stmt->execute(array('tName'=>$this->name,
                                        'tEmail'=>$this->email,
                                        'tNickName'=>$this->nickName));


        if($response){
            //header('Location:index.php');
            Massage::message("<div class=\"alert alert-success\">
  <strong>Congratulation!</strong> Your Data has been stored successfully.
</div>");
            Utility::redirect('index.php');
        }
        else{
            Massage::message("<div class=\"alert alert-danger\">
  <strong>Sorry!</strong> You are fail to store data.
</div>");
            Utility::redirect('index.php');
        }
//    if($res) {
//
//        echo"Yes";
//    }
//       else {
//            echo "sorry!";
//        }
     }
    public function index()
    {
        $dataquery="SELECT * FROM `trainer`";
        $stmt=$this->conn->query($dataquery);
        $alldata=$stmt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }
    public function view(){
        $sqlquery="SELECT * FROM `trainer` WHERE `id`=:setid";
        $stmt=$this->conn->prepare($sqlquery);
        $stmt->execute(array(':setid'=>$this->id));
        $finaldata=$stmt->fetch(PDO::FETCH_OBJ);
        return $finaldata;
    }


}