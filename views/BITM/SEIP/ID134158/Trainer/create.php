<?php
session_start();
include_once("../../../../../vendor/autoload.php");
use App\BITM\SEIP\ID134158\Massage\Massage;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>labExam8</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Trainer Information</h2>
    <form method="post" action="store.php">
        <div class="form-group">
            <label for="tname">Trainer Name:</label>
            <input type="text" class="form-control" id="tname" name="tname" placeholder="Enter Trainer name">
        </div>
        <div class="form-group">
            <label for="temail">Trainer Email:</label>
            <input type="email" class="form-control" id="temail" name="temail" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="tnick">Trainer Nick Name:</label>
            <input type="text" class="form-control" id="tnick" name="tnick"  placeholder="Enter Nick name">
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>

