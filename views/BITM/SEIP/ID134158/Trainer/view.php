<?php

include_once('../../../../../vendor/autoload.php');
use \App\BITM\SEIP\ID134158\Trainer\Trainer;

$obj= new Trainer();
$result=$obj->setData($_GET)->view();



?>



<!DOCTYPE html>
<html lang="en">
<head>
    <title>Labexam8</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Trainer Individual Data</h2>
    <ul class="list-group">
        <li class="list-group-item">ID: <?php echo $result->id ?></li>
        <li class="list-group-item">Trainer name: <?php echo $result->trainer_name ?></li>
        <li class="list-group-item">Trainer Email:<?php echo $result->trainer_email ?></li>
        <li class="list-group-item">Trainer Nick Name:<?php echo $result->trainer_nick_name ?></li>
    </ul>
</div>

</body>
</html>