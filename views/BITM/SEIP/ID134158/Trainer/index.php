<?php
session_start();
include_once("../../../../../vendor/autoload.php");
use App\BITM\SEIP\ID134158\Trainer\Trainer;
use App\BITM\SEIP\ID134158\Massage\Massage;

$obj= new Trainer();
$allResult= $obj->index();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Labexam8</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    </br>
    </br>
    <div id="message">
        <?php
        if(array_key_exists('message',$_SESSION) and !empty($_SESSION['message'])){
           echo Massage::message();
        }
        ?>
</div>

<a href="create.php" class="btn btn-primary" role="button">Upadate</a>



<h2>Trainer Info</h2>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>SL</th>
        <th>ID</th>
        <th>Trainer Name</th>
        <th>Trainer Email</th>
        <th>Trainer Nick Name</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $sl=0;
    foreach($allResult as $result){
        $sl++?>
        <tr>
            <td><?php echo $sl?></td>
            <td><?php echo $result->id ?></td>
            <td><?php echo $result->trainer_name ?></td>
            <td><?php echo $result->trainer_email ?></td>
            <td><?php echo $result->trainer_nick_name ?></td>
            <td>
                <a href="view.php?id=<?php echo $result->id ?>" class="btn btn-primary" role="button">View</a>
                <a href="edit.php" class="btn btn-warning" role="button">Edit</a>
                <a href="delete.php" class="btn btn-danger" role="button">Delete</a>
            </td>
        </tr>
    <?php } ?>

    </tbody>
</table>
</div>
<script>
    $("#message").show().delay(5000).fadeOut();
</script>

</body>
</html>